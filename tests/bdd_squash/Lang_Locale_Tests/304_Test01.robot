# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_304_SETUP}	Get Variable Value	${TEST 304 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_304_SETUP is not None	${__TEST_304_SETUP}

Test Teardown
	${__TEST_304_TEARDOWN}	Get Variable Value	${TEST 304 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_304_TEARDOWN is not None	${__TEST_304_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
Test01
	[Setup]	Test Setup

	Given the test step

	[Teardown]	Test Teardown
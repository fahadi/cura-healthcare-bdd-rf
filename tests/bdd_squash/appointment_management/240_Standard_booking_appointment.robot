# Automation priority: null
# Test case importance: Medium
*** Settings ***
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_240_SETUP}	Get Variable Value	${TEST 240 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_240_SETUP is not None	${__TEST_240_SETUP}

Test Teardown
	${__TEST_240_TEARDOWN}	Get Variable Value	${TEST 240 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_240_TEARDOWN is not None	${__TEST_240_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
Standard booking appointment
	${facility} =	Get Test Param	DS_facility
	${readmission} =	Get Test Param	DS_readmission
	${healthcare} =	Get Test Param	DS_healthcare
	${date} =	Get Test Param	DS_date
	${comment} =	Get Test Param	DS_comment

	[Setup]	Test Setup

	Given I am logged out
	When I navigate to the MakeAppointment Page
	And I login with demo values
	And I choose facility ${facility}, readmission ${readmission}, healthcare program ${healthcare}, date ${date}, comment ${comment} and book an appointment
	Then The appointment with facility ${facility}, readmission ${readmission}, healthcare program ${healthcare}, date ${date} and comment ${comment} is booked

	[Teardown]	Test Teardown
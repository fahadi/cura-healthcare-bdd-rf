# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource
Library		squash_tf.TFParamService

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_277_SETUP}	Get Variable Value	${TEST 277 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_277_SETUP is not None	${__TEST_277_SETUP}

Test Teardown
	${__TEST_277_TEARDOWN}	Get Variable Value	${TEST 277 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_277_TEARDOWN is not None	${__TEST_277_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
DEMO_CommunAutom
	${facility} =	Get Test Param	DS_facility
	${readmission} =	Get Test Param	DS_readmission
	${healthcare} =	Get Test Param	DS_healthcare
	${date} =	Get Test Param	DS_date
	${comment} =	Get Test Param	DS_comment

	${row_1_1} =	Create List	facility	readmission	healthcare	date	comment
	${row_1_2} =	Create List	Tokyo CURA Healthcare Center	N	None	02/02/2023	annual check up
	${datatable_1} =	Create List	${row_1_1}	${row_1_2}

	${docstring_1} =	Set Variable	annual check up

	[Setup]	Test Setup

	Given I am logged out
	When I navigate to the MakeAppointment Page
	And I login with demo values
	And I choose facility "Tokyo CURA Healthcare Center", readmission "N", healthcare program "None", date "02/02/2023", comment "annual check up" and book an appointment
	Then The appointment with facility "Tokyo CURA Healthcare Center", readmission "N", healthcare program "None", date "02/02/2023" and comment "annual check up" is booked
	And The comment is "${docstring_1}"
	And Those appointments are booked "${datatable_1}"
	When I navigate to the MakeAppointment Page
	And I choose facility ${facility}, readmission ${readmission}, healthcare program ${healthcare}, date ${date}, comment ${comment} and book an appointment
	Then The appointment with facility ${facility}, readmission ${readmission}, healthcare program ${healthcare}, date ${date} and comment ${comment} is booked

	[Teardown]	Test Teardown
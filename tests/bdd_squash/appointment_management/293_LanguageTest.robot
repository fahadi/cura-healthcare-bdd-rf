# Automation priority: 58
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_293_SETUP}	Get Variable Value	${TEST 293 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_293_SETUP is not None	${__TEST_293_SETUP}

Test Teardown
	${__TEST_293_TEARDOWN}	Get Variable Value	${TEST 293 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_293_TEARDOWN is not None	${__TEST_293_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
LanguageTest
	[Setup]	Test Setup

	Given test
	Then test succeeded

	[Teardown]	Test Teardown
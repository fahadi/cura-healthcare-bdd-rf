# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_245_SETUP}	Get Variable Value	${TEST 245 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_245_SETUP is not None	${__TEST_245_SETUP}

Test Teardown
	${__TEST_245_TEARDOWN}	Get Variable Value	${TEST 245 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_245_TEARDOWN is not None	${__TEST_245_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
KO test
	[Setup]	Test Setup

	Given I am logged out
	When I navigate to the MakeAppointment Page
	And I login with demo values
	And I choose facility "Tokyo CURA Healthcare Center", readmission "N", healthcare program "None", date "01/02/2023", comment "check up" and book an appointment
	Then The appointment with facility "Seoul CURA Healthcare Center", readmission "N", healthcare program "None", date "02/02/2023" and comment "check up" is booked

	[Teardown]	Test Teardown
# Automation priority: null
# Test case importance: Medium
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_242_SETUP}	Get Variable Value	${TEST 242 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_242_SETUP is not None	${__TEST_242_SETUP}

Test Teardown
	${__TEST_242_TEARDOWN}	Get Variable Value	${TEST 242 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_242_TEARDOWN is not None	${__TEST_242_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
Cannot book when date is not set
	[Setup]	Test Setup

	Given I am logged out
	When I navigate to the MakeAppointment Page
	And I login with demo values
	And I choose facility "Tokyo CURA Healthcare Center", readmission "N", healthcare program "none", date "-", comment "check up" and book an appointment
	Then The error message "Veuillez renseigner ce champ" is displayed

	[Teardown]	Test Teardown